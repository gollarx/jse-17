package ru.t1.shipilov.tm.api.service;

import ru.t1.shipilov.tm.api.repository.ITaskRepository;
import ru.t1.shipilov.tm.enumerated.Sort;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {

    List<Task> findAll(Sort sort);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}
