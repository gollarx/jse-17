package ru.t1.shipilov.tm.api.model;

import ru.t1.shipilov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
