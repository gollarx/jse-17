package ru.t1.shipilov.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear();
    }

    @Override
    public String getDescription() {
        return "Delete all projects.";
    }

    @Override
    public String getName() {
        return "project-clear";
    }

}
