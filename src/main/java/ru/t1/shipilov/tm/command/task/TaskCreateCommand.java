package ru.t1.shipilov.tm.command.task;

import ru.t1.shipilov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(name, description);
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public String getName() {
        return "task-create";
    }

}
