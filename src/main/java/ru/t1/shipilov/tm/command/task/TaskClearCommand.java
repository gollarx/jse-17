package ru.t1.shipilov.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        getTaskService().clear();
    }

    @Override
    public String getDescription() {
        return "Delete all tasks.";
    }

    @Override
    public String getName() {
        return "task-clear";
    }

}
